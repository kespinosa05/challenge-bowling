package ec.com.bowling;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertTrue;


public class BowlingConsoleApplicationITTest {

    private transient final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private transient final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private transient final PrintStream originalOut = System.out;
    private transient final PrintStream originalErr = System.err;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    public void shouldSystemOutPrintlnWhenPlayerPlayPerfectScore() throws IOException {
        BowlingConsoleApplication.main(new String[]{"src/test/resources/valid/sampleInput1PlayerPerfectScore.txt"});
        File fileOutput = new File(Thread.currentThread().getContextClassLoader().getResource("valid/sampleInput1PlayerPerfectScoreOutput.txt").getFile());
        String validOutput = FileUtils.readFileToString(fileOutput, StandardCharsets.UTF_8);

        assertTrue(outContent.toString().contains(validOutput));
    }

    @Test
    public void shouldSystemOutPrintlnWhenPlayerPlay0Score() throws IOException {
        BowlingConsoleApplication.main(new String[]{"src/test/resources/valid/sampleInput0Score.txt"});
        File fileOutput = new File(Thread.currentThread().getContextClassLoader().getResource("valid/sampleInput0ScoreOutput.txt").getFile());
        String validOutput = FileUtils.readFileToString(fileOutput, StandardCharsets.UTF_8);

        assertTrue(outContent.toString().contains(validOutput));
    }

    @Test
    public void shouldSystemOutPrintlnWhen2PlayerPlay() throws IOException {
        BowlingConsoleApplication.main(new String[]{"src/test/resources/valid/sampleInput2Player.txt"});
        File fileOutput = new File(Thread.currentThread().getContextClassLoader().getResource("valid/sampleInput2PlayerOutput.txt").getFile());
        String validOutput = FileUtils.readFileToString(fileOutput, StandardCharsets.UTF_8);

        assertTrue(outContent.toString().contains(validOutput));
    }

    @Test
    public void shouldSystemOutPrintlnWhenPlayerPlayAllFaultScore() throws IOException {
        BowlingConsoleApplication.main(new String[]{"src/test/resources/valid/sampleInputAllFaultScore.txt"});
        File fileOutput = new File(Thread.currentThread().getContextClassLoader().getResource("valid/sampleInputAllFaultScoreOutput.txt").getFile());
        String validOutput = FileUtils.readFileToString(fileOutput, StandardCharsets.UTF_8);

        assertTrue(outContent.toString().contains(validOutput));
    }

    @Test
    public void shouldSystemOutPrintlnWhen8PlayerPlaySomeScore() throws IOException {
        BowlingConsoleApplication.main(new String[]{"src/test/resources/valid/sampleInput8Player.txt"});
        File fileOutput = new File(Thread.currentThread().getContextClassLoader().getResource("valid/sampleInput8PlayerOutput.txt").getFile());
        String validOutput = FileUtils.readFileToString(fileOutput, StandardCharsets.UTF_8);


        System.out.println(validOutput.toString());

        System.out.println(outContent.toString());

        assertTrue(outContent.toString().contains(validOutput));
    }

    @Test
    public void shouldSystemOutErrorWhenNoArgs() {
        BowlingConsoleApplication.main(new String[]{});
        assertTrue(errContent.toString().contains("Error: Invalid arguments"));
    }

    @Test
    public void shouldSystemOutErrorWhenMoreThan1Args() {
        BowlingConsoleApplication.main(new String[]{"fakepath.txt", "fakepath2.txt"});
        assertTrue(errContent.toString().contains("Error: Invalid arguments"));
    }

    @Test
    public void shouldSystemOutErrorWhenNoValidArgs() {
        BowlingConsoleApplication.main(new String[]{"fake.txt"});

        assertTrue(errContent.toString().contains("Error: java.nio.file.NoSuchFileException:"));
    }

}

