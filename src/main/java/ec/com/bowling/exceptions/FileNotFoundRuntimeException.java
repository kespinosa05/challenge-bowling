package ec.com.bowling.exceptions;

import java.io.IOException;

/**
 * @author kenny lopez
 * @version 1.0.0
 */
public class FileNotFoundRuntimeException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public FileNotFoundRuntimeException(IOException e) {
        super(e);
    }
}

