package ec.com.bowling.exceptions;

/**
 * @author kenny lopez
 * @version 1.0.0
 */
public class InvalidRollRuntimeException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public InvalidRollRuntimeException(String msg) {
        super(msg);
    }
}

