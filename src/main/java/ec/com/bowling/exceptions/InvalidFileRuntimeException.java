package ec.com.bowling.exceptions;

/**
 * @author kenny lopez
 * @version 1.0.0
 */
public class InvalidFileRuntimeException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public InvalidFileRuntimeException(String msg) {
        super(msg);
    }
}

