package ec.com.bowling.utils;

import lombok.experimental.UtilityClass;

import java.util.regex.Pattern;

/**
 * Constants util.
 *
 * @author kenny lopez
 * @version 1.0.0
 */
@UtilityClass
public class Constants {

    public static final String FAIL_VALUE = "F";
    public static final Integer MAX_FRAME_ALLOWED = 10;
    public static final Integer MAX_SCORING_ALLOWED = 10;

    public static final String PINFALL_STRIKE = "X";
    public static final String PINFALL_SPARE = "/";

    public static final String OUTPUT_FORMAT_TAB = "\t";
    public static final String OUTPUT_FORMAT_END_LINE = "\n";

    /**
     * REGEX for validate input line format.
     */
    public static final Pattern PATTERN = Pattern.compile("(^\\p{L}+[\\p{L}\\p{Z}\\p{P}]{0,})[\\s]+(" + FAIL_VALUE + "|10|[0-9])$");

    public static final Integer PATTERN_GROUP_NAME = 1;
    public static final Integer PATTERN_GROUP_VALUE = 2;

    public static final String OUTPUT_FRAME_LABEL = "Frames";
    public static final String OUTPUT_PINFALLS_LABEL = "Pinfalls";
    public static final String OUTPUT_SCORES_LABEL = "Scores";

}
