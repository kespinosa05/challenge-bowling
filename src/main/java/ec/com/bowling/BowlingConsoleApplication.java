package ec.com.bowling;

import ec.com.bowling.exceptions.FileNotFoundRuntimeException;
import ec.com.bowling.exceptions.InvalidFileFormatException;
import ec.com.bowling.exceptions.InvalidFileRuntimeException;
import ec.com.bowling.exceptions.InvalidRollRuntimeException;
import ec.com.bowling.services.BowlingChallengeService;
import ec.com.bowling.services.impl.BowlingGameServiceImpl;
import ec.com.bowling.services.impl.BowlingGameOutputServiceImpl;
import ec.com.bowling.services.impl.InputScoringServiceImpl;
import ec.com.bowling.services.impl.BowlingChallengeConsoleServiceImpl;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.File;

/**
 * Main application class.
 *
 * @author kenny lopez
 * @version 1.0.0
 */
@Slf4j
@UtilityClass
public class BowlingConsoleApplication {

    /**
     * Run main app.
     *
     * @param args arguments passed.
     */
    public static void main(String[] args) {
        log.debug("main: {}", (Object) args);
        try {
            if (StringUtils.isAllEmpty(args) || args.length != 1) {
                throw new InvalidFileRuntimeException("Invalid arguments");
            }
            File file = new File(args[0]);
            BowlingChallengeService bowlingChallenge = new BowlingChallengeConsoleServiceImpl(new BowlingGameServiceImpl(), new BowlingGameOutputServiceImpl(),
                    new InputScoringServiceImpl());

            bowlingChallenge.challenge(file);
        } catch (FileNotFoundRuntimeException | InvalidFileFormatException | InvalidFileRuntimeException | InvalidRollRuntimeException e) {
            log.error("Error: {}", e.getLocalizedMessage());
        }
    }
}
