package ec.com.bowling.services;

import ec.com.bowling.models.InputScoring;

import java.io.File;
import java.util.List;

/**
 * Input scoring service.
 *
 * @author kenny lopez
 * @version 1.0.0
 */
public interface InputScoringService {
    /**
     * Process File with roll scoring.
     *
     * @param file File to process
     * @return A list of input scoring processed.
     */
    List<InputScoring> processInputFile(File file);
}
