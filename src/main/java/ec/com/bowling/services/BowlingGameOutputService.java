package ec.com.bowling.services;

import ec.com.bowling.models.BowlingGame;

public interface BowlingGameOutputService {

    /**
     * Build the output format of the bowling game result.
     *
     * @param bowlingGame
     * @return A string output formatted
     */
    String buildOutput(BowlingGame bowlingGame);
}
