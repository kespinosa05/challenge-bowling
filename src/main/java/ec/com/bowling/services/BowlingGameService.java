package ec.com.bowling.services;

import ec.com.bowling.models.BowlingGame;
import ec.com.bowling.models.InputScoring;

import java.util.List;

/**
 * Bowling game service.
 *
 * @author kenny lopez
 * @version 1.0.0
 */
public interface BowlingGameService {

    /**
     * Play bowling game.
     *
     * @param inputScorings Input Scoring
     * @return A bowling game instance
     */
    BowlingGame playGame(List<InputScoring> inputScorings);

}
