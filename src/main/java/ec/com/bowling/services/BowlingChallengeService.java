package ec.com.bowling.services;

import java.io.File;

/**
 * Challenge service implementation.
 *
 * @author kenny lopez
 * @version 1.0.0
 */
public interface BowlingChallengeService {
    /**
     * Challenge.
     *
     * @param file input file
     */
    void challenge(File file);
}
