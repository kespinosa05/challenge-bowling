package ec.com.bowling.services.impl;

import ec.com.bowling.models.BowlingGame;
import ec.com.bowling.models.IFrame;
import ec.com.bowling.services.BowlingGameOutputService;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static ec.com.bowling.utils.Constants.*;

/**
 * Output service implementation.
 *
 * @author kenny lopez
 * @version 1.0.0
 */
public class BowlingGameOutputServiceImpl implements BowlingGameOutputService {

    @Override
    public String buildOutput(BowlingGame bowlingGame) {
        StringBuilder stringBuilder = new StringBuilder();
        List<Integer> frames = IntStream.rangeClosed(1, MAX_FRAME_ALLOWED).boxed().collect(Collectors.toList());
        String framesHeader = OUTPUT_FRAME_LABEL + OUTPUT_FORMAT_TAB + OUTPUT_FORMAT_TAB + frames.stream().map(Object::toString).
                collect(Collectors.joining(OUTPUT_FORMAT_TAB + OUTPUT_FORMAT_TAB));
        stringBuilder.append(framesHeader);
        stringBuilder.append(OUTPUT_FORMAT_END_LINE);
        bowlingGame.getLines().forEach(line -> {
            stringBuilder.append(line.getPlayerName() + OUTPUT_FORMAT_END_LINE);
            stringBuilder.append(OUTPUT_PINFALLS_LABEL + OUTPUT_FORMAT_TAB + line.getFrames().stream().map(IFrame::getPinfall).
                    collect(Collectors.joining(OUTPUT_FORMAT_TAB)) + OUTPUT_FORMAT_END_LINE);
            stringBuilder.append(OUTPUT_SCORES_LABEL + OUTPUT_FORMAT_TAB + OUTPUT_FORMAT_TAB + frames.stream().map(i -> String.valueOf(line.getScore(i))).
                    collect(Collectors.joining(OUTPUT_FORMAT_TAB + OUTPUT_FORMAT_TAB)));
            stringBuilder.append(OUTPUT_FORMAT_END_LINE);
        });
        stringBuilder.setLength(stringBuilder.length() - 1);
        return stringBuilder.toString();
    }
}
