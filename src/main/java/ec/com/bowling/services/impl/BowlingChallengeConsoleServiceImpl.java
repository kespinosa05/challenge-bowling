package ec.com.bowling.services.impl;

import ec.com.bowling.models.BowlingGame;
import ec.com.bowling.models.InputScoring;
import ec.com.bowling.services.BowlingChallengeService;
import ec.com.bowling.services.BowlingGameOutputService;
import ec.com.bowling.services.BowlingGameService;
import ec.com.bowling.services.InputScoringService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import java.io.File;
import java.util.List;

/**
 * Bowling challenge service implementation.
 *
 * @author kenny lopez
 * @version 1.0.0
 */
@Slf4j
@AllArgsConstructor
public class BowlingChallengeConsoleServiceImpl implements BowlingChallengeService {

    private BowlingGameService bowlingGameService;
    private BowlingGameOutputService outputService;
    private InputScoringService inputScoringService;

    @Override
    public void challenge(File file){
        List<InputScoring> processInputFile = inputScoringService.processInputFile(file);
        BowlingGame bowlingGame = bowlingGameService.playGame(processInputFile);
        String output = outputService.buildOutput(bowlingGame);
        System.out.println(output);
    }
}
