package ec.com.bowling.services.impl;

import ec.com.bowling.exceptions.FileNotFoundRuntimeException;
import ec.com.bowling.exceptions.InvalidFileFormatException;
import ec.com.bowling.models.InputScoring;
import ec.com.bowling.services.InputScoringService;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

import static ec.com.bowling.utils.Constants.*;

/**
 * Input scoring service implementation.
 *
 * @author kenny lopez
 * @version 1.0.0
 */
@Slf4j
public class InputScoringServiceImpl implements InputScoringService {
    @Override
    public List<InputScoring> processInputFile(File file) {
        log.debug("processInputFile: {}, Exist: {}", file.getAbsolutePath(), file.exists());
        try (final BufferedReader br = Files.newBufferedReader(Paths.get(file.getAbsolutePath()))) {
            return br.lines().map(this::convertLineToInputScoring).collect(Collectors.toList());
        } catch (IOException e) {
            throw new FileNotFoundRuntimeException(e);
        }
    }

    private InputScoring convertLineToInputScoring(String line) {
        log.debug("readLine: {}", line);
        Matcher matcher = PATTERN.matcher(line);
        if (matcher.matches()) {
            String scoring = matcher.group(PATTERN_GROUP_VALUE);
            Integer scoringValue;
            Boolean fault;
            if (FAIL_VALUE.equals(scoring)) {
                scoringValue = 0;
                fault = Boolean.TRUE;
            } else {
                scoringValue = Integer.valueOf(scoring);
                fault = Boolean.FALSE;
            }
            return InputScoring.builder().playerName(matcher.group(PATTERN_GROUP_NAME).trim()).scoring(scoringValue).fault(fault).build();
        } else {
            throw new InvalidFileFormatException("Invalid format line, The correct format is Player name tab character and F or a number between 0 and 10.");
        }
    }
}
