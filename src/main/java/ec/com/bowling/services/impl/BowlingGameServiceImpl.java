package ec.com.bowling.services.impl;

import ec.com.bowling.exceptions.InvalidRollRuntimeException;
import ec.com.bowling.models.BowlingGame;
import ec.com.bowling.models.InputScoring;
import ec.com.bowling.services.BowlingGameService;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * Bowling game service implementation.
 *
 * @author kenny lopez
 * @version 1.0.0
 */
@Slf4j
public class BowlingGameServiceImpl implements BowlingGameService {

    @Override
    public BowlingGame playGame(List<InputScoring> inputScorings) {
        BowlingGame bowlingGame = new BowlingGame();
        inputScorings.forEach(bowlingGame::roll);
        if (!bowlingGame.allLineAreDone()) {
            throw new InvalidRollRuntimeException("Game incomplete.");
        }
        return bowlingGame;
    }

}