package ec.com.bowling.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import static ec.com.bowling.utils.Constants.*;

/**
 * Frame logic.
 *
 * @author kenny lopez
 * @version 1.0.0
 */
@Getter
@Setter
@AllArgsConstructor
@Builder
public class Frame implements IFrame {
    protected Throw throwOne;
    protected Throw throwTwo;

    @Override
    public boolean isStrike() {
        return throwOne == null ? Boolean.FALSE : MAX_SCORING_ALLOWED.equals(throwOne.getScoring());
    }

    @Override
    public boolean isSpare() {
        return !isStrike() && (throwOne == null ? 0 : throwOne.getScoring()) + (throwTwo == null ? 0 : throwTwo.getScoring()) == MAX_FRAME_ALLOWED;
    }

    @Override
    public boolean isValid() {
        return (throwOne == null ? 0 : throwOne.getScoring()) + (throwTwo == null ? 0 : throwTwo.getScoring()) <= MAX_SCORING_ALLOWED;
    }

    @Override
    public String getPinfall() {
        if (isStrike()) {
            return OUTPUT_FORMAT_TAB + PINFALL_STRIKE;
        } else if (isSpare()) {
            return this.throwOne.getPinfall() + OUTPUT_FORMAT_TAB + PINFALL_SPARE;
        } else {
            return this.throwOne.getPinfall() + OUTPUT_FORMAT_TAB + this.throwTwo.getPinfall();
        }
    }

    @Override
    public Integer getLocalScore() {
        return isStrike() ? this.throwOne.getScoring() : (this.throwOne.getScoring() + this.throwTwo.getScoring());
    }

    @Override
    public boolean allThrowsAreDone() {
        return isStrike() ? Boolean.TRUE : this.throwOne != null && this.throwTwo != null;
    }
}
