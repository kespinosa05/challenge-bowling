package ec.com.bowling.models;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.LinkedList;
import java.util.List;

import static ec.com.bowling.utils.Constants.MAX_FRAME_ALLOWED;

/**
 * @author kenny lopez
 * @version 1.0.0
 */
@Getter
@Setter
@Builder
public class Line {
    private final List<IFrame> frames = new LinkedList<>();
    private String playerName;

    public boolean lineIsDone() {
        return frames.size() == MAX_FRAME_ALLOWED && getLastFrame().allThrowsAreDone();
    }

    public LastFrame getLastFrame() {
        return frames.size() != MAX_FRAME_ALLOWED ? null : (LastFrame) getFrames().get(MAX_FRAME_ALLOWED - 1);
    }

    public IFrame getLastFramePlayed() {
        return frames.isEmpty() ? null : getFrames().get(getFrames().size() - 1);
    }

    public Integer getScore(Integer frame) {
        IFrame currentFrame = frames.get(frame - 1);
        Integer cant = currentFrame.getLocalScore();
        cant += getScoreExtraPoint(frame, currentFrame);
        if (frame.equals(1)) {
            return cant;
        } else {
            return cant + getScore(frame - 1);
        }
    }

    private Integer getScoreExtraPoint(Integer position, IFrame currentFrame) {
        Integer cant = 0;
        if (currentFrame instanceof LastFrame) {
            return cant;
        } else if (currentFrame.isSpare()) {
            cant += frames.get(position).getThrowOne().getScoring();
        } else if (currentFrame.isStrike()) {
            IFrame nextFrame = frames.get(position);
            cant += nextFrame.getThrowOne().getScoring();
            if (nextFrame instanceof LastFrame || !nextFrame.isStrike()) {
                cant += nextFrame.getThrowTwo().getScoring();
            } else {
                IFrame next2Frame = frames.get(position + 1);
                cant += next2Frame.getThrowOne().getScoring();
            }
        }
        return cant;
    }

}