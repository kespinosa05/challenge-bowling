package ec.com.bowling.models;

/**
 * Interface behavior frame.
 *
 * @author kenny lopez
 * @version 1.0.0
 */
public interface IFrame {
    /**
     * Check if frame is a strike.
     *
     * @return true if is strike or false is not.
     */
    boolean isStrike();

    /**
     * Check if frame is a spare.
     *
     * @return true if is spare or false is not.
     */
    boolean isSpare();

    /**
     * Check if frame is valid.
     *
     * @return true if is valid or false is not.
     */
    boolean isValid();

    /**
     * Get pinfall format.
     *
     * @return pinfall format output
     */
    String getPinfall();

    /**
     * Return value local score without bonus.
     *
     * @return Value of local score without
     */
    Integer getLocalScore();

    /**
     * Check if frame has all throws done.
     *
     * @return true if is valid or false is not.
     */
    boolean allThrowsAreDone();

    /**
     * Return throw one.
     *
     * @return throw one instance.
     */
    Throw getThrowOne();

    /**
     * Set throw one.
     *
     * @param throwOne Throw one instance.
     */
    void setThrowOne(Throw throwOne);

    /**
     * Return throw two.
     *
     * @return throw two instance.
     */
    Throw getThrowTwo();

    /**
     * Set throw two.
     *
     * @param throwTwo Throw two instance.
     */
    void setThrowTwo(Throw throwTwo);
}
