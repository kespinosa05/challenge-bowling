package ec.com.bowling.models;

import ec.com.bowling.exceptions.InvalidRollRuntimeException;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static ec.com.bowling.utils.Constants.MAX_FRAME_ALLOWED;

/**
 * Bowling game logic.
 *
 * @author kenny lopez
 * @version 1.0.0
 */
@Getter
public class BowlingGame {
    private List<Line> lines = new ArrayList<>();

    public boolean allLineAreDone() {
        return lines.stream().allMatch(Line::lineIsDone);
    }

    /**
     * Roll a player.
     *
     * @param input Scoring when player roll.
     */
    public void roll(InputScoring input) {
        Optional<Line> lineFound = findLinePerPlayer(input.getPlayerName());
        Line line;
        IFrame currentFrame;
        if (lineFound.isPresent()) {
            line = lineFound.get();
            if (line.lineIsDone()) {
                throw new InvalidRollRuntimeException("Roll not allowed");
            }
            currentFrame = line.getLastFramePlayed();
            if (currentFrame == null || currentFrame.allThrowsAreDone()) {
                currentFrame = buildFrameWithThrowOne(line, input);
                line.getFrames().add(currentFrame);
            } else {
                if (isRollBonusExtra(currentFrame)) {
                    ((LastFrame) currentFrame).setThrowThree(Throw.builder().scoring(input.getScoring()).fault(input.isFault()).build());
                } else {
                    currentFrame.setThrowTwo(Throw.builder().scoring(input.getScoring()).fault(input.isFault()).build());
                }
            }
        } else {
            line = Line.builder().playerName(input.getPlayerName()).build();
            addPLayer(line);
            currentFrame = buildFrameWithThrowOne(line, input);
            line.getFrames().add(currentFrame);
        }

        if (!currentFrame.isValid()) {
            throw new InvalidRollRuntimeException("Roll scoring not allowed");
        }
    }

    public Optional<Line> findLinePerPlayer(String playerName) {
        return lines.stream().filter(p -> p.getPlayerName().equals(playerName)).findFirst();
    }

    private void addPLayer(Line player) {
        this.lines.add(player);
    }

    private boolean isRollBonusExtra(IFrame currentFrame) {
        return currentFrame instanceof LastFrame && currentFrame.getThrowOne() != null && currentFrame.getThrowTwo() != null;
    }

    private Frame buildFrameWithThrowOne(Line line, InputScoring input) {
        Throw throwOne = Throw.builder().scoring(input.getScoring()).fault(input.isFault()).build();
        if (line.getFrames().size() == MAX_FRAME_ALLOWED - 1) {
            return new LastFrame(throwOne);
        } else {
            return Frame.builder().throwOne(throwOne).build();
        }
    }

}
