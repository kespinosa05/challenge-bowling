package ec.com.bowling.models;

import ec.com.bowling.utils.Constants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * @author kenny lopez
 * @version 1.0.0
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
public class Throw {
    @Builder.Default
    private Integer scoring = 0;
    private boolean fault;

    public String getPinfall() {
        return (isFault() ? Constants.FAIL_VALUE : String.valueOf(this.scoring));
    }
}
