package ec.com.bowling.models;

import lombok.Getter;
import lombok.Setter;

import static ec.com.bowling.utils.Constants.*;

/**
 * Last Frame logic.
 *
 * @author kenny lopez
 * @version 1.0.0
 */
@Getter
@Setter
public class LastFrame extends Frame {
    private Throw throwThree;

    public LastFrame(Throw scoringOne) {
        super(scoringOne, null);
    }

    @Override
    public boolean isValid() {
        return !isStrike() ? super.isValid() : Boolean.TRUE;
    }

    @Override
    public String getPinfall() {
        String pinfall = (getPinfallStrike(throwOne)) + OUTPUT_FORMAT_TAB;
        pinfall += MAX_SCORING_ALLOWED.equals(throwTwo.getScoring()) ? PINFALL_STRIKE : getPinfall(throwOne.getScoring() + throwTwo.getScoring(), PINFALL_SPARE, throwTwo);
        pinfall += this.throwThree == null ? "" : OUTPUT_FORMAT_TAB + getPinfallStrike(this.throwThree);
        return pinfall;
    }

    private String getPinfall(Integer scoring, String pinfall, Throw th) {
        return MAX_SCORING_ALLOWED.equals(scoring) ? pinfall : th.getPinfall();
    }

    private String getPinfallStrike(Throw th) {
        return getPinfall(th.getScoring(), PINFALL_STRIKE, th);
    }


    @Override
    public Integer getLocalScore() {
        return (this.throwOne.getScoring() + this.throwTwo.getScoring()) + (this.throwThree == null ? 0 : this.throwThree.getScoring());
    }

    @Override
    public boolean allThrowsAreDone() {
        return (needExtraThrow() && this.throwThree != null) || (!needExtraThrow() && this.throwOne != null && this.throwTwo != null && this.throwThree == null);
    }

    private boolean needExtraThrow() {
        return isStrike() || isSpare();
    }
}
