package ec.com.bowling.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * @author kenny lopez
 * @version 1.0.0
 */
@AllArgsConstructor
@Getter
@Setter
@Builder
public class InputScoring {
    private String playerName;
    private Integer scoring;
    private boolean fault;
}
