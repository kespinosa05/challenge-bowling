package ec.com.bowling.services.impl;

import com.github.javafaker.Faker;
import ec.com.bowling.exceptions.FileNotFoundRuntimeException;
import ec.com.bowling.exceptions.InvalidFileFormatException;
import ec.com.bowling.models.InputScoring;
import ec.com.bowling.services.InputScoringService;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class InputScoringServiceImplTest {

    private transient Faker faker;
    private transient String playerName;
    private transient InputScoringService inputScoringService;

    @Before
    public void setUp() {
        faker = new Faker();
        playerName = faker.name().firstName();

        inputScoringService = new InputScoringServiceImpl();
    }

    @Test
    public void shouldReturnCompletedListInputScoring() {
        File file = new File(Thread.currentThread().getContextClassLoader().getResource("valid/sampleInput0Score.txt").getFile());

        List<InputScoring> inputScorings = inputScoringService.processInputFile(file);

        assertThat(inputScorings.size(), is(20));
    }

    @Test
    public void shouldReturnCompletedListInputScoringWithFaultTrue() {
        File file = new File(Thread.currentThread().getContextClassLoader().getResource("valid/sampleInputAllFaultScore.txt").getFile());

        List<InputScoring> inputScorings = inputScoringService.processInputFile(file);

        MatcherAssert.assertThat(inputScorings.size(), Is.is(20));
        inputScorings.forEach(i -> {
            assertThat(i.isFault(), is(true));
        });

    }

    @Test(expected = FileNotFoundRuntimeException.class)
    public void shouldThrowFileNotFoundRuntimeExceptionWhenProcessInputFileFileNotExist() {
        inputScoringService.processInputFile(new File("fakefile.txt"));
    }

    @Test(expected = InvalidFileFormatException.class)
    public void shouldThrowInvalidFileFormatExceptionWhenProcessInputFileInvalidFormatScoring() {
        File file = new File(Thread.currentThread().getContextClassLoader().getResource("invalid/sampleInputInvalidFormatInvalidScoring.txt").getFile());

        inputScoringService.processInputFile(file);
    }

    @Test(expected = InvalidFileFormatException.class)
    public void shouldThrowInvalidFileFormatExceptionWhenProcessInputFileInvalidFormatSeparator() {
        File file = new File(Thread.currentThread().getContextClassLoader().getResource("invalid/sampleInputInvalidFormatInvalidSeparator.txt").getFile());

        inputScoringService.processInputFile(file);
    }

    @Test(expected = InvalidFileFormatException.class)
    public void shouldThrowInvalidFileFormatExceptionWhenProcessInputFileInvalidFormatPlayerName() {
        File file = new File(Thread.currentThread().getContextClassLoader().getResource("invalid/sampleInputInvalidFormatInvalidPlayerName.txt").getFile());

        inputScoringService.processInputFile(file);
    }


}