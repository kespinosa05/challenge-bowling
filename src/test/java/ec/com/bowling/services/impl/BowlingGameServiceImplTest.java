package ec.com.bowling.services.impl;

import com.github.javafaker.Faker;
import ec.com.bowling.exceptions.InvalidRollRuntimeException;
import ec.com.bowling.models.BowlingGame;
import ec.com.bowling.models.InputScoring;
import ec.com.bowling.services.BowlingGameService;
import ec.com.bowling.services.BowlingGameOutputService;
import ec.com.bowling.services.InputScoringService;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class BowlingGameServiceImplTest {

    private transient Faker faker;
    private transient String playerName;
    private transient BowlingGameService bowlingGameService;
    private transient BowlingGameOutputService outputService;
    private transient InputScoringService inputScoringService;

    @Before
    public void setUp() {
        faker = new Faker();
        playerName = faker.name().firstName();

        bowlingGameService = new BowlingGameServiceImpl();
        outputService = new BowlingGameOutputServiceImpl();
        inputScoringService = new InputScoringServiceImpl();
    }

    @Test(expected = InvalidRollRuntimeException.class)
    public void shouldThrownWhenPlayGameNotCompleted() {
        bowlingGameService.playGame(Arrays.asList(InputScoring.builder().playerName(playerName).fault(true).scoring(0).build(), InputScoring.builder().
                playerName(playerName).scoring(faker.number().randomDigit()).build()));
    }

    @Test
    public void shouldReturnValidGameWithValidInput() {

        File file = new File(Thread.currentThread().getContextClassLoader().getResource("valid/sampleInput2Player.txt").getFile());

        BowlingGame bowlingGame = bowlingGameService.playGame(inputScoringService.processInputFile(file));

        Assert.assertNotNull(bowlingGame);
        assertThat(bowlingGame.allLineAreDone(), is(true));
    }


    @Test
    public void shouldReturnValidOutPutFormat2Player() throws IOException {

        File file = new File(Thread.currentThread().getContextClassLoader().getResource("valid/sampleInput2Player.txt").getFile());

        File fileOutput = new File(Thread.currentThread().getContextClassLoader().getResource("valid/sampleInput2PlayerOutput.txt").getFile());
        String validOutput = FileUtils.readFileToString(fileOutput, StandardCharsets.UTF_8);

        BowlingGame bowlingGame = bowlingGameService.playGame(inputScoringService.processInputFile(file));
        String output = outputService.buildOutput(bowlingGame);
        Assert.assertNotNull(output);
        assertThat(output, is(validOutput));
    }

    @Test
    public void shouldReturnValidOutPutFormat0Score() throws IOException {

        File file = new File(Thread.currentThread().getContextClassLoader().getResource("valid/sampleInput0Score.txt").getFile());

        File fileOutput = new File(Thread.currentThread().getContextClassLoader().getResource("valid/sampleInput0ScoreOutput.txt").getFile());
        String validOutput = FileUtils.readFileToString(fileOutput, StandardCharsets.UTF_8);

        BowlingGame bowlingGame = bowlingGameService.playGame(inputScoringService.processInputFile(file));
        String output = outputService.buildOutput(bowlingGame);

        Assert.assertNotNull(output);
        assertThat(output, is(validOutput));
    }

    @Test
    public void shouldReturnValidOutPutFormatPerfectScore() throws IOException {

        File file = new File(Thread.currentThread().getContextClassLoader().getResource("valid/sampleInput1PlayerPerfectScore.txt").getFile());

        File fileOutput = new File(Thread.currentThread().getContextClassLoader().getResource("valid/sampleInput1PlayerPerfectScoreOutput.txt").getFile());
        String validOutput = FileUtils.readFileToString(fileOutput, StandardCharsets.UTF_8);

        BowlingGame bowlingGame = bowlingGameService.playGame(inputScoringService.processInputFile(file));
        String output = outputService.buildOutput(bowlingGame);

        Assert.assertNotNull(output);
        assertThat(output, is(validOutput));
    }


    @Test
    public void shouldReturnValidOutPutFormatallFaultScore() throws IOException {

        File file = new File(Thread.currentThread().getContextClassLoader().getResource("valid/sampleInputAllFaultScore.txt").getFile());

        File fileOutput = new File(Thread.currentThread().getContextClassLoader().getResource("valid/sampleInputAllFaultScoreOutput.txt").getFile());
        String validOutput = FileUtils.readFileToString(fileOutput, StandardCharsets.UTF_8);

        BowlingGame bowlingGame = bowlingGameService.playGame(inputScoringService.processInputFile(file));
        String output = outputService.buildOutput(bowlingGame);

        Assert.assertNotNull(output);
        assertThat(output, is(validOutput));
    }


}