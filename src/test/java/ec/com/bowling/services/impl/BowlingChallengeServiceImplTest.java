package ec.com.bowling.services.impl;

import com.github.javafaker.Faker;
import ec.com.bowling.exceptions.FileNotFoundRuntimeException;
import ec.com.bowling.exceptions.InvalidFileFormatException;
import ec.com.bowling.models.BowlingGame;
import ec.com.bowling.models.InputScoring;
import ec.com.bowling.services.BowlingGameOutputService;
import ec.com.bowling.services.BowlingGameService;
import ec.com.bowling.services.InputScoringService;
import ec.com.bowling.utils.Constants;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BowlingChallengeServiceImplTest {

    private transient Faker faker;
    private transient String playerName;
    private transient List<InputScoring> inputScorings;
    private transient BowlingGame bowlingGame;
    private transient String bowlingGameOutput;

    @InjectMocks
    private transient BowlingChallengeConsoleServiceImpl bowlingChallengeConsoleService;

    @Mock
    private transient BowlingGameService bowlingGameService;
    @Mock
    private transient BowlingGameOutputService outputService;
    @Mock
    private transient InputScoringService inputScoringService;

    private transient final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private transient final PrintStream originalOut = System.out;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Before
    public void setUp() {
        faker = new Faker();
        playerName = faker.name().firstName();

        inputScorings = Arrays.asList(InputScoring.builder().scoring(faker.number().randomDigit()).build());
        bowlingGame = new BowlingGame();
        bowlingGameOutput = faker.random().hex();

        when(inputScoringService.processInputFile(any())).thenReturn(inputScorings);

        when(bowlingGameService.playGame(inputScorings)).thenReturn(bowlingGame);

        when(outputService.buildOutput(bowlingGame)).thenReturn(bowlingGameOutput);
    }

    @Test
    public void shouldCallAllServices(){
        File file = new File("anypath");

        this.bowlingChallengeConsoleService.challenge(file);

        verify(inputScoringService, times(1)).processInputFile(file);
        verify(bowlingGameService, times(1)).playGame(inputScorings);
        verify(outputService, times(1)).buildOutput(bowlingGame);
    }

    @Test
    public void shouldPrintlnOutputFormat(){
        File file = new File("anypath");

        this.bowlingChallengeConsoleService.challenge(file);

        assertThat(outContent.toString(),is(bowlingGameOutput + Constants.OUTPUT_FORMAT_END_LINE));
    }


}