package ec.com.bowling.models;

import com.github.javafaker.Faker;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;

import static ec.com.bowling.utils.Constants.MAX_FRAME_ALLOWED;
import static ec.com.bowling.utils.Constants.MAX_SCORING_ALLOWED;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class FrameTest {

    private transient Faker faker;

    @Before
    public void setUp() {
        faker = new Faker();
    }

    @Test
    public void shouldFalseIsStrikeFrameWhenScoringThrowOneIsNotMaximum() {
        Frame frame = Frame.builder().throwOne(Throw.builder().scoring(faker.number().numberBetween(0, MAX_FRAME_ALLOWED - 1)).build()).build();

        assertFalse(frame.isStrike());
    }

    @Test
    public void shouldTrueIsStrikeFrameWhenScoringThrowOneIsMaximum() {
        Frame frame = Frame.builder().throwOne(Throw.builder().scoring(MAX_SCORING_ALLOWED).build()).build();

        assertTrue(frame.isStrike());
    }

    @Test
    public void shouldFalseIsSpareFrameWhenSumOfScoringIsNotMaximum() {
        Frame frame = Frame.builder().throwOne(Throw.builder().scoring(3).build()).throwTwo(Throw.builder().scoring(3).build()).build();

        assertFalse(frame.isSpare());
    }

    @Test
    public void shouldTrueIsSpareFrameWhenSumOfScoringIsMaximum() {
        Frame frame = Frame.builder().throwOne(Throw.builder().scoring(5).build()).throwTwo(Throw.builder().scoring(5).build()).build();

        assertTrue(frame.isSpare());
    }

    @Test
    public void shouldTrueIsValidFrameWhenSumOfScoringIsInRangeAllowedAndFrameIsStrike() {
        Frame frame = Frame.builder().throwOne(Throw.builder().scoring(MAX_SCORING_ALLOWED).build()).build();

        assertTrue(frame.isValid());
    }

    @Test
    public void shouldTrueIsValidFrameWhenSumOfScoringIsInRangeAllowed() {
        Frame frame = Frame.builder().throwOne(Throw.builder().scoring(5).build()).throwTwo(Throw.builder().scoring(4).build()).build();

        assertTrue(frame.isValid());
    }

    @Test
    public void shouldFalseIsValidFrameWhenSumOfScoringExceededRangeAllowed() {
        Frame frame = Frame.builder().throwOne(Throw.builder().scoring(5).build()).throwTwo(Throw.builder().scoring(7).build()).build();

        assertFalse(frame.isValid());
    }

    @Test
    public void shouldTrueIsValidFrameWhenThrowOneIsNull() {
        Frame frame = Frame.builder().build();

        assertTrue(frame.isValid());
    }

    @Test
    public void shouldStrikeFormatIsWhenIsMaximumScoring() {
        String expectedPinfall = "\t" + "X";
        Frame frame = Frame.builder().throwOne(Throw.builder().scoring(MAX_SCORING_ALLOWED).build()).build();

        String actualPinfall = frame.getPinfall();

        MatcherAssert.assertThat(actualPinfall, Is.is(expectedPinfall));
    }

    @Test
    public void shouldSpareFormatIsWhenIsSpareThrow() {
        String expectedPinfall = "5" + "\t/";
        Frame frame = Frame.builder().throwOne(Throw.builder().scoring(5).build()).throwTwo(Throw.builder().scoring(5).build()).build();

        String actualPinfall = frame.getPinfall();

        MatcherAssert.assertThat(actualPinfall, Is.is(expectedPinfall));
    }

    @Test
    public void shouldNormalFormatIsWhenAreNotSpareAndStrikeThrow() {
        String expectedPinfall = "4" + "\t" + "5";
        Frame frame = Frame.builder().throwOne(Throw.builder().scoring(4).build()).throwTwo(Throw.builder().scoring(5).build()).build();

        String actualPinfall = frame.getPinfall();

        MatcherAssert.assertThat(actualPinfall, Is.is(expectedPinfall));
    }

    @Test
    public void shouldMaximumLocalScoreWhenThrowIsStrike() {
        Frame frame = Frame.builder().throwOne(Throw.builder().scoring(MAX_SCORING_ALLOWED).build()).build();

        Integer actualLocalScore = frame.getLocalScore();

        MatcherAssert.assertThat(actualLocalScore, Is.is(MAX_SCORING_ALLOWED));
    }

    @Test
    public void shouldLocalScoreWhenThrowIsNotStrike() {
        Frame frame = Frame.builder().throwOne(Throw.builder().scoring(4).build()).throwTwo(Throw.builder().scoring(5).build()).build();

        Integer actualLocalScore = frame.getLocalScore();

        MatcherAssert.assertThat(actualLocalScore, Is.is(9));
    }

    @Test
    public void shouldTrueWhenAllThrowsAreDoneAndFrameIsStrike() {
        Frame frame = Frame.builder().throwOne(Throw.builder().scoring(MAX_SCORING_ALLOWED).build()).build();

        assertTrue(frame.allThrowsAreDone());
    }

    @Test
    public void shouldTrueWhenAllThrowsAreDone() {
        Frame frame = Frame.builder().throwOne(Throw.builder().scoring(5).build()).throwTwo(Throw.builder().scoring(5).build()).build();

        assertTrue(frame.allThrowsAreDone());
    }

    @Test
    public void shouldFalseWhenThrowTwoIsNull() {
        Frame frame = Frame.builder().throwOne(Throw.builder().scoring(5).build()).build();

        assertFalse(frame.allThrowsAreDone());
    }

    @Test
    public void shouldFalseWhenThrowOneIsNull() {
        Frame frame = Frame.builder().throwTwo(Throw.builder().scoring(5).build()).build();

        assertFalse(frame.allThrowsAreDone());
    }
}