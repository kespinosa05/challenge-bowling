package ec.com.bowling.models;

import com.github.javafaker.Faker;
import ec.com.bowling.utils.Constants;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;

import static ec.com.bowling.utils.Constants.MAX_SCORING_ALLOWED;

public class ThrowTest {
    private transient Faker faker;

    @Before
    public void setUp() {
        faker = new Faker();
    }

    @Test
    public void shouldFailValueWhenRollIsFault() {
        Throw throwNumber = Throw.builder().fault(true).scoring(0).build();

        String expectedPinfall = throwNumber.getPinfall();

        MatcherAssert.assertThat(expectedPinfall, Is.is(Constants.FAIL_VALUE));
    }

    @Test
    public void shouldScoringValueWhenRollIsOk() {
        Integer value = faker.number().numberBetween(0, MAX_SCORING_ALLOWED);
        Throw throwNumber = Throw.builder().fault(false).scoring(value).build();

        String expectedPinfall = throwNumber.getPinfall();

        MatcherAssert.assertThat(expectedPinfall, Is.is(String.valueOf(value)));
    }
}