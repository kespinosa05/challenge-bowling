package ec.com.bowling.models;

import com.github.javafaker.Faker;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

import static ec.com.bowling.utils.Constants.MAX_FRAME_ALLOWED;
import static ec.com.bowling.utils.Constants.MAX_SCORING_ALLOWED;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;

public class LineTest {

    private transient Faker faker;

    @Before
    public void setUp() {
        faker = new Faker();
    }

    @Test
    public void shouldNullWhenGetFrameIsEmpty() {
        Line line = new Line(faker.name().firstName());

        IFrame expectedLastFrame = line.getLastFramePlayed();

        assertNull(expectedLastFrame);
    }

    @Test
    public void shouldFrameWhenGetFrameIsNotNull() {
        Line line = new Line(faker.name().firstName());
        Frame frame = Frame.builder().throwOne(Throw.builder().scoring(3).fault(false).build()).build();
        line.getFrames().add(frame);

        IFrame expectedLastFrame = line.getLastFramePlayed();

        assertEquals(expectedLastFrame, frame);
    }

    @Test
    public void shouldReturnTrueWhenFrameAreCompletedAndAllThrowsAreDone() {
        Line line = new Line(faker.name().firstName());
        line.getFrames().addAll(getCompleteListFrame());

        boolean lineDoneExpected = line.lineIsDone();

        assertTrue(lineDoneExpected);
    }

    @Test
    public void shouldReturnFalseWhenAllFrameAreNotCompleted() {
        Line line = new Line(faker.name().firstName());
        line.getFrames().addAll(getFrameListWithOutLastFrame());

        boolean lineDoneExpected = line.lineIsDone();

        assertFalse(lineDoneExpected);
    }

    @Test
    public void shouldReturnFalseWhenAllThrowsAreNotDone() {
        Line line = new Line(faker.name().firstName());
        IntStream.rangeClosed(1, MAX_FRAME_ALLOWED - 1).boxed().forEach(i -> {
            line.getFrames().add(Frame.builder().throwOne(Throw.builder().scoring(MAX_FRAME_ALLOWED).build()).build());
        });
        //No completed last frame
        line.getFrames().add(new LastFrame(Throw.builder().scoring(faker
                .number().numberBetween(0, MAX_FRAME_ALLOWED - 1)).build()));

        boolean lineDoneExpected = line.lineIsDone();

        assertFalse(lineDoneExpected);
    }

    @Test
    public void shouldReturnNullWhenLineWithoutAnyFrame() {
        Line line = new Line(faker.name().firstName());

        LastFrame lastFrame = line.getLastFrame();

        assertNull(lastFrame);
    }

    @Test
    public void shouldReturnNullWhenLineNotHasLastFrame() {
        Line line = new Line(faker.name().firstName());
        IntStream.rangeClosed(1, MAX_FRAME_ALLOWED - 1).boxed().forEach(i -> {
            line.getFrames().add(Frame.builder().throwOne(Throw.builder().scoring(MAX_FRAME_ALLOWED).build()).build());
        });

        LastFrame lastFrame = line.getLastFrame();

        assertNull(lastFrame);
    }

    @Test
    public void shouldReturnLastFrameWhenLineHasAllFrame() {
        Line line = new Line(faker.name().firstName());
        IntStream.rangeClosed(1, MAX_FRAME_ALLOWED - 1).boxed().forEach(i -> {
            line.getFrames().add(Frame.builder().throwOne(Throw.builder().scoring(MAX_FRAME_ALLOWED).build()).build());
        });
        // last frame
        line.getFrames().add(new LastFrame(Throw.builder().scoring(faker
                .number().numberBetween(0, MAX_FRAME_ALLOWED)).build()));

        LastFrame lastFrame = line.getLastFrame();

        assertNotNull(lastFrame);
    }

    @Test
    public void shouldGetScoreWhenFrameIsSpare() {
        Line line = new Line(faker.name().firstName());
        line.getFrames().addAll(getCompleteListFrame());

        Integer actualScore = line.getScore(4);

        assertThat(actualScore, Is.is(89));
    }

    @Test
    public void shouldGetScoreWhenFrameIsStrike() {
        Line line = new Line(faker.name().firstName());
        line.getFrames().addAll(getCompleteListFrame());

        Integer actualScore = line.getScore(1);

        assertThat(actualScore, Is.is(30));
    }

    private List<Frame> getCompleteListFrame() {
        List<Frame> frameList = getFrameListWithOutLastFrame();
        LastFrame lastFrame = getLastFrame();
        frameList.add(lastFrame);
        return frameList;
    }

    private LastFrame getLastFrame() {
        Throw throwOne = Throw.builder().scoring(MAX_FRAME_ALLOWED).fault(false).build();
        LastFrame lastFrame = new LastFrame(throwOne);
        lastFrame.setThrowTwo(throwOne);
        lastFrame.setThrowThree(throwOne);
        return lastFrame;
    }

    private Frame getStrikeFrame() {
        Throw throwOne = Throw.builder().scoring(MAX_SCORING_ALLOWED).fault(false).build();
        return Frame.builder().throwOne(throwOne).build();
    }

    private Frame getSpareFrame() {
        Throw throwOne = Throw.builder().scoring(5).fault(false).build();
        return Frame.builder().throwOne(throwOne).throwTwo(throwOne).build();
    }

    private List<Frame> getFrameListWithOutLastFrame() {
        List<Frame> frameList = new LinkedList<>();
        Throw throwOne = Throw.builder().scoring(4).fault(false).build();
        Frame frame = Frame.builder().throwOne(throwOne).throwTwo(throwOne).build();
        frameList.add(getStrikeFrame());
        frameList.add(getStrikeFrame());
        frameList.add(getStrikeFrame());
        frameList.add(getSpareFrame());
        IntStream.rangeClosed(1, 5).boxed().forEach(i -> {
            frameList.add(frame);
        });
        return frameList;
    }
}