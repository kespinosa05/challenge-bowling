package ec.com.bowling.models;

import com.github.javafaker.Faker;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static ec.com.bowling.utils.Constants.MAX_SCORING_ALLOWED;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LastFrameTest {

    private transient Faker faker;

    @Before
    public void setUp() {
        faker = new Faker();
    }

    @Test
    public void shouldFalseIsStrikeLastFrame() {
        LastFrame lastFrame = new LastFrame(Throw.builder().scoring(faker.number().numberBetween(0, MAX_SCORING_ALLOWED - 1)).build());
        Assert.assertFalse(lastFrame.isStrike());
    }

    @Test
    public void shouldTrueIsValidlastFrame() {
        LastFrame lastFrame = new LastFrame(Throw.builder().scoring(faker.number().numberBetween(0, MAX_SCORING_ALLOWED)).build());

        Assert.assertTrue(lastFrame.isValid());
    }

    @Test
    public void shouldXIsWhenIsMaximumScoring() {
        String expectedPinfall = "X" + "\t" + "X" + "\t" + "X";
        LastFrame lastFrame = getLastFrameStrike();

        String actualPinfall = lastFrame.getPinfall();

        MatcherAssert.assertThat(actualPinfall, Is.is(expectedPinfall));
    }

    @Test
    public void shouldMaximumLocalScore() {
        LastFrame lastFrame = getLastFrameStrike();

        Integer actualLocalScore = lastFrame.getLocalScore();

        MatcherAssert.assertThat(actualLocalScore, Is.is(30));
    }

    @Test
    public void shouldTrueWhenAllThrowsAreDoneAndLastFrameIsStrike() {
        LastFrame lastFrame = getLastFrameStrike();

        assertTrue(lastFrame.allThrowsAreDone());
    }

    @Test
    public void shouldTrueWhenAllThrowsAreDoneAndLastFrameIsSpare() {
        LastFrame lastFrame = getLastFrameSpare();

        assertTrue(lastFrame.allThrowsAreDone());
    }

    @Test
    public void shouldTrueWhenAllThrowsAreDoneAndLastFrameIsNotStrikeAndSpare() {
        LastFrame lastFrame = getLastFrame();

        assertTrue(lastFrame.allThrowsAreDone());
    }

    @Test
    public void shouldFalseWhenNotAllThrowsAreDone() {
        LastFrame lastFrame = new LastFrame(Throw.builder().scoring(0).build());

        assertFalse(lastFrame.allThrowsAreDone());
    }

    @Test
    public void shouldFalseWhenThrowOneIsNull() {
        LastFrame lastFrame = new LastFrame(null);

        assertFalse(lastFrame.allThrowsAreDone());
    }

    private LastFrame getLastFrame() {
        Throw throwOne = Throw.builder().scoring(3).fault(false).build();
        LastFrame lastFrame = new LastFrame(throwOne);
        lastFrame.setThrowTwo(throwOne);
        lastFrame.setThrowThree(null);
        return lastFrame;
    }

    private LastFrame getLastFrameSpare() {
        Throw throwOne = Throw.builder().scoring(5).fault(false).build();
        LastFrame lastFrame = new LastFrame(throwOne);
        lastFrame.setThrowTwo(throwOne);
        lastFrame.setThrowThree(throwOne);
        return lastFrame;
    }

    private LastFrame getLastFrameStrike() {
        Throw throwOne = Throw.builder().scoring(MAX_SCORING_ALLOWED).fault(false).build();
        LastFrame lastFrame = new LastFrame(throwOne);
        lastFrame.setThrowTwo(throwOne);
        lastFrame.setThrowThree(throwOne);
        return lastFrame;
    }
}