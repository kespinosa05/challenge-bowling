package ec.com.bowling.models;

import com.github.javafaker.Faker;
import ec.com.bowling.exceptions.InvalidRollRuntimeException;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

import static ec.com.bowling.utils.Constants.MAX_FRAME_ALLOWED;
import static ec.com.bowling.utils.Constants.MAX_SCORING_ALLOWED;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class BowlingGameTest {
    private transient Faker faker;
    private transient String playerName;

    @Before
    public void setUp() {
        faker = new Faker();
        playerName = faker.name().firstName();
    }

    @Test
    public void shouldReturnLineWhenPlayerIsFound() {
        BowlingGame bowlingGame = new BowlingGame();
        bowlingGame.getLines().addAll(getLines());
        Optional<Line> line = bowlingGame.findLinePerPlayer(playerName);

        assertThat(line.get().getPlayerName(), Is.is(playerName));
    }

    @Test
    public void shouldReturnEmptyLineWhenPlayerIsNotFound() {
        BowlingGame bowlingGame = new BowlingGame();
        bowlingGame.getLines().addAll(getLines());
        Optional<Line> line = bowlingGame.findLinePerPlayer(faker.name().firstName());

        assertThat(line, Is.is(Optional.empty()));
    }

    @Test
    public void shouldReturnFalseWhenAllLineAreNotDone() {
        BowlingGame bowlingGame = new BowlingGame();
        List<Line> lines = new LinkedList<>();
        lines.add(new Line(playerName));
        bowlingGame.getLines().addAll(lines);

        assertFalse(bowlingGame.allLineAreDone());
    }

    @Test
    public void shouldReturnTrueWhenAllLineAreDone() {
        BowlingGame bowlingGame = new BowlingGame();

        bowlingGame.getLines().addAll(getLines());

        assertTrue(bowlingGame.allLineAreDone());
    }

    @Test
    public void shouldAddNewLineWhenRollNewPlayer() {
        BowlingGame bowlingGame = new BowlingGame();
        InputScoring inputScoring = InputScoring.builder().playerName(faker.name().firstName()).scoring(faker.number()
                .numberBetween(0, MAX_SCORING_ALLOWED)).build();

        bowlingGame.roll(inputScoring);

        assertThat(bowlingGame.getLines().size(), is(1));
    }

    @Test
    public void shouldSameCountLineForSameCountPlayer() {
        BowlingGame bowlingGame = new BowlingGame();
        Integer playerCount = faker.number().randomDigit();
        IntStream.rangeClosed(1, playerCount).boxed().forEach(player -> {
            String playerName = faker.name().firstName();
            InputScoring inputScoring = InputScoring.builder().playerName(playerName).scoring(5).build();
            InputScoring inputScoring2 = InputScoring.builder().playerName(playerName).scoring(4).build();

            bowlingGame.roll(inputScoring);
            bowlingGame.roll(inputScoring2);

        });
        assertThat(bowlingGame.getLines().size(), is(playerCount));
    }


    @Test
    public void shouldFirstRollInFirstFrame() {
        BowlingGame bowlingGame = new BowlingGame();
        Integer scoring = faker.number().numberBetween(0, MAX_SCORING_ALLOWED);
        InputScoring inputScoring = InputScoring.builder().playerName(faker.name().firstName()).scoring(scoring).build();

        bowlingGame.roll(inputScoring);

        assertThat(bowlingGame.getLines().size(), is(1));
        assertThat(bowlingGame.getLines().get(0).getFrames().size(), is(1));
        assertNotNull(bowlingGame.getLines().get(0).getFrames().get(0).getThrowOne());
        assertThat(bowlingGame.getLines().get(0).getFrames().get(0).getThrowOne().getScoring(), is(scoring));
    }

    @Test
    public void shouldSecondRollInFirstFrame() {
        BowlingGame bowlingGame = new BowlingGame();
        String playerName = faker.name().firstName();
        Integer scoring = 6;
        InputScoring inputScoring = InputScoring.builder().playerName(playerName).scoring(scoring).build();
        Integer scoring2 = 3;
        InputScoring inputScoring2 = InputScoring.builder().playerName(playerName).scoring(scoring2).build();

        bowlingGame.roll(inputScoring);
        bowlingGame.roll(inputScoring2);

        assertThat(bowlingGame.getLines().size(), is(1));
        assertThat(bowlingGame.getLines().get(0).getFrames().size(), is(1));
        assertNotNull(bowlingGame.getLines().get(0).getFrames().get(0).getThrowTwo());
        assertThat(bowlingGame.getLines().get(0).getFrames().get(0).getThrowTwo().getScoring(), is(scoring2));
    }

    @Test
    public void shouldJumpFrameWhenCurrentFrameIsCompleted() {
        BowlingGame bowlingGame = new BowlingGame();
        String playerName = faker.name().firstName();
        Integer scoring = 4;
        InputScoring inputScoring = InputScoring.builder().playerName(playerName).scoring(scoring).build();
        Integer scoring2 = 5;
        InputScoring inputScoring2 = InputScoring.builder().playerName(playerName).scoring(scoring2).build();

        Integer scoring3 = faker.number().numberBetween(0, MAX_SCORING_ALLOWED);
        InputScoring inputScoring3 = InputScoring.builder().playerName(playerName).scoring(scoring3).build();

        bowlingGame.roll(inputScoring);
        bowlingGame.roll(inputScoring2);
        bowlingGame.roll(inputScoring3);

        assertThat(bowlingGame.getLines().size(), is(1));
        assertThat(bowlingGame.getLines().get(0).getFrames().size(), is(2));
        assertNotNull(bowlingGame.getLines().get(0).getFrames().get(1).getThrowOne());
        assertThat(bowlingGame.getLines().get(0).getFrames().get(1).getThrowOne().getScoring(), is(scoring3));
    }

    @Test(expected = InvalidRollRuntimeException.class)
    public void shouldThrowInvalidRollExceptionWhenRollThrowExtraRoll() {
        BowlingGame bowlingGame = new BowlingGame();
        String playerName = faker.name().firstName();
        IntStream.rangeClosed(1, MAX_FRAME_ALLOWED + 2).boxed().map(i -> InputScoring.builder().playerName(playerName).scoring(MAX_SCORING_ALLOWED).build()).forEach(bowlingGame::roll);

        InputScoring extraInputScoring = InputScoring.builder().playerName(playerName).scoring(faker.number()
                .numberBetween(0, MAX_SCORING_ALLOWED)).build();

        bowlingGame.roll(extraInputScoring);
    }

    @Test(expected = InvalidRollRuntimeException.class)
    public void shouldThrowInvalidRollExceptionWhenRollFrameIsSumMoreThanMaximumAllowed() {
        BowlingGame bowlingGame = new BowlingGame();
        String playerName = faker.name().firstName();

        InputScoring inputScoring = InputScoring.builder().playerName(playerName).scoring(5).build();
        InputScoring inputScoring2 = InputScoring.builder().playerName(playerName).scoring(6).build();

        bowlingGame.roll(inputScoring);
        bowlingGame.roll(inputScoring2);
    }

    @Test
    public void shouldLastFrameExtraRoll() {
        BowlingGame bowlingGame = new BowlingGame();

        IntStream.rangeClosed(1, MAX_FRAME_ALLOWED + 2).boxed().map(i -> InputScoring.builder().playerName(playerName).scoring(MAX_SCORING_ALLOWED).build()).forEach(bowlingGame::roll);

        assertNotNull(bowlingGame.getLines().get(0).getLastFrame().getThrowThree());
    }

    private List<Line> getLines() {
        List<Line> lines = new LinkedList<>();
        Line line = new Line(playerName);
        line.getFrames().addAll(getCompleteListFrame());
        lines.add(line);
        return lines;
    }

    private List<Frame> getCompleteListFrame() {
        List<Frame> frameList = getFrameListWithOutLastFrame();
        LastFrame lastFrame = getLastFrame();
        frameList.add(lastFrame);
        return frameList;
    }

    private LastFrame getLastFrame() {
        Throw throwOne = Throw.builder().scoring(MAX_FRAME_ALLOWED).fault(false).build();
        LastFrame lastFrame = new LastFrame(throwOne);
        lastFrame.setThrowTwo(throwOne);
        lastFrame.setThrowThree(throwOne);
        return lastFrame;
    }

    private Frame getStrikeFrame() {
        Throw throwOne = Throw.builder().scoring(MAX_SCORING_ALLOWED).fault(false).build();
        return Frame.builder().throwOne(throwOne).build();
    }

    private Frame getSpareFrame() {
        Throw throwOne = Throw.builder().scoring(5).fault(false).build();
        return Frame.builder().throwOne(throwOne).throwTwo(throwOne).build();
    }

    private List<Frame> getFrameListWithOutLastFrame() {
        List<Frame> frameList = new LinkedList<>();
        Throw throwOne = Throw.builder().scoring(4).fault(false).build();
        Frame frame = Frame.builder().throwOne(throwOne).throwTwo(throwOne).build();
        frameList.add(getStrikeFrame());
        frameList.add(getStrikeFrame());
        frameList.add(getStrikeFrame());
        frameList.add(getSpareFrame());
        IntStream.rangeClosed(1, 5).boxed().forEach(i -> {
            frameList.add(frame);
        });
        return frameList;
    }

}