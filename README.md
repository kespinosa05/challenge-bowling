### Requirement ###
For build and run need `Java 8` and `Gradle 6.0+` or  gradle grapper`./gradlew`

### build jar ###
Must run command ```gradle clean build shadowJar```

### Run jar ###
Arguments most be only one and can be absolute path or relative path to excutable jar

```
java -jar build/libs/ec.com.challenge-bowling-1.0.0-SNAPSHOT-all.jar src/test/resources/valid/sampleInput8Player.txt
java -jar build/libs/ec.com.challenge-bowling-1.0.0-SNAPSHOT-all.jar src/test/resources/valid/sampleInput0Score.txt
java -jar build/libs/ec.com.challenge-bowling-1.0.0-SNAPSHOT-all.jar src/test/resources/valid/sampleInput1PlayerPerfectScore.txt
java -jar build/libs/ec.com.challenge-bowling-1.0.0-SNAPSHOT-all.jar src/test/resources/valid/sampleInput2Player.txt
java -jar build/libs/ec.com.challenge-bowling-1.0.0-SNAPSHOT-all.jar src/test/resources/valid/sampleInputAllFaultScore.txt
```

### Run integration test ###
Run gradle command ```gradle integrationTest```

### Run into IntelliJ IDEA IDE ###
For run in IntelliJ IDEA most be active annotation processor

### Code coverage ###
For show code coverage of unit test run ```gradle jacocoTestReport```
For show code coverage of unit test and integration test run ```gradle build jacocoTestReport```
 
### Analysis of static code with sonarqube ###
 
 Run command ```gradle sonarqube``` or download sonar scanner from https://docs.sonarqube.org/latest/analysis/scan/sonarscanner/
 and run ```sonar-scanner```
 
 ### Security analysis with OWASP ###
 For identifies known vulnerable dependencies used by the project most run ```gradle dependencyCheckAnalyze```
 
 
 
[![Build Status](https://jenkinsserver/buildStatus/icon?job=ec.com.bowling:ec.com.challenge-bowling)](https://gitlab.com/kespinosa05/challenge-bowling)

[![Quality gate](https://sonarcloud.io/api/project_badges/measure?project=ec.com.bowling:ec.com.challenge-bowling&metric=alert_status)](https://sonarcloud.io/dashboard?id=ec.com.bowling:ec.com.challenge-bowling)

[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=ec.com.bowling:ec.com.challenge-bowling&metric=coverage)](https://sonarcloud.io/component_measures?id=ec.com.bowling:ec.com.challenge-bowling&metric=coverage&view=list)

[![Technical deb](https://sonarcloud.io/api/project_badges/measure?project=ec.com.bowling:ec.com.challenge-bowling&metric=sqale_index)](https://sonarcloud.io/component_measures?id=ec.com.bowling:ec.com.challenge-bowling&metric=sqale_index&view=list)

[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=ec.com.bowling:ec.com.challenge-bowling&metric=vulnerabilities)](https://sonarcloud.io/component_measures?id=ec.com.bowling:ec.com.challenge-bowling&metric=vulnerabilities&view=list)

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=ec.com.bowling:ec.com.challenge-bowling&metric=bugs)](https://sonarcloud.io/component_measures?id=ec.com.bowling:ec.com.challenge-bowling&metric=bugs&view=list)

[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=ec.com.bowling:ec.com.challenge-bowling&metric=code_smells)](https://sonarcloud.io/component_measures?id=ec.com.bowling:ec.com.challenge-bowling&metric=code_smells&view=list)

[![ncloc](https://sonarcloud.io/api/project_badges/measure?project=ec.com.bowling:ec.com.challenge-bowling&metric=ncloc)](https://sonarcloud.io/component_measures?id=ec.com.bowling:ec.com.challenge-bowling&metric=ncloc&view=list)

[![duplicated_lines_density](https://sonarcloud.io/api/project_badges/measure?project=ec.com.bowling:ec.com.challenge-bowling&metric=duplicated_lines_density)](https://sonarcloud.io/component_measures?id=ec.com.bowling:ec.com.challenge-bowling&metric=duplicated_lines_density&view=list)

[![Security rating](https://sonarcloud.io/api/project_badges/measure?project=ec.com.bowling:ec.com.challenge-bowling&metric=security_rating)](https://sonarcloud.io/component_measures?id=ec.com.bowling:ec.com.challenge-bowling&metric=security_rating&view=list)

[![Reliability rating](https://sonarcloud.io/api/project_badges/measure?project=ec.com.bowling:ec.com.challenge-bowling&metric=reliability_rating)](https://sonarcloud.io/component_measures?id=ec.com.bowling:ec.com.challenge-bowling&metric=reliability_rating&view=list)

[![Sqale rating](https://sonarcloud.io/api/project_badges/measure?project=ec.com.bowling:ec.com.challenge-bowling&metric=sqale_rating)](https://sonarcloud.io/component_measures?id=ec.com.bowling:ec.com.challenge-bowling&metric=sqale_rating&view=list)

